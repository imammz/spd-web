<?php

namespace App\Http\Controllers;

use App\Helpers\ApiServices;
use Illuminate\Http\Request;
use Faker\Factory as Faker;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class PublicController extends Controller
{
    public function index() {
        $faker = Faker::create();
        $data = [];
        $data['faker'] = $faker;
        $data['topik'] = ApiServices::_reqTopikWithSub();
        $data['lp'] = ApiServices::_getLandingPage();

       

        $artikel = [];
        foreach($data['topik'] as $topik) {
            foreach($topik->sub as $sub) {
                if(isset($sub->KodeSubTopik)) {
                    $artikel[] = ApiServices::_getArtikelList($topik->KodeTopik,$sub->KodeSubTopik,1,20);
                }
                else {
                    $artikel[] = array();
                }
               
            }
            
        }



       foreach($artikel as $row) {
           if(isset($row->Result)) {

            if(isset($row->Result->ARTIKEL->PageIndex)) {
                $getArtikel = [0=>$row->Result->ARTIKEL];
            }
            else {
                if(isset($row->Result->ARTIKEL)) {
                    $getArtikel = $row->Result->ARTIKEL;
                }
                else {
                    $getArtikel = [];
                }
               
            }
            $data['artikel'][] = $getArtikel;
           }
       }


        return view('beranda',$data);

    }

    public function topik($id = 0,$KodeSubTopik = 0) {

        $faker = Faker::create();
        $data = [];
        $data['faker'] = $faker;
        $data['KodeTopik'] = $id;
        $data['topik'] = ApiServices::_reqTopikWithSub();
        $data['subtopik'] = ApiServices::_getSubtopikByIdTopik($id);


        $subtopik = $data['subtopik'];
        $subtopik_default = 0;

        if(isset($subtopik->Result->SubTopik)) {
            $get_default = $subtopik->Result->SubTopik;
            $get_default = $get_default[0];
            $subtopik_default = $get_default->KodeSubTopik;
            if($KodeSubTopik == 0) {
                $KodeSubTopik = $subtopik_default; 
            }
        }

        $data['KodeSubTopik'] = $KodeSubTopik;
      
        $artikel = ApiServices::_getArtikelList($id,$KodeSubTopik,1,200);
      
        if(isset($artikel->Result->ARTIKEL->PageIndex)) {
            $data['artikel'] = [0=>$artikel->Result->ARTIKEL];
        }
        else {
            if(isset($artikel->Result->ARTIKEL)) {
                $data['artikel'] = $artikel->Result->ARTIKEL;
            }
            else {
                $data['artikel'] = [];
            }
           
        }
      

        return view('topik',$data);

    }

    public function artikel($id = 0,$idsub = 0, $id_content = 0) {

        $data = [];
        $data['topik'] = ApiServices::_reqTopikWithSub();
        $data['KodeTopik'] = $id;
        $data['KodeSubTopik'] = $idsub;
        $data['KodeArtikel'] = $id_content;
        $data['artikel'] = ApiServices::_reqArtikelWithKodeTopik($id);
        $data['subtopik'] = ApiServices::_getSubtopikByIdTopik($id);

        $artikelDetail = ApiServices::_getArtikelById($id,$idsub,$id_content);
        $data['artikelDetail'] = $artikelDetail;

        $urlArtikel = $artikelDetail->Result->ARTIKEL->ArtikelKontenURL;
        

        $client = new Client();
        $response = $client->request('GET', $urlArtikel, []);
        $content = $response->getBody()->getContents();

        $content = str_replace('HEIGHT: 521px','HEIGHT: 421px',$content);
        $content = str_replace('WIDTH: 900px','WIDTH: 600px',$content);
      
        if($id==8) {
            $content = str_replace('<img','<img class="gallery-thumb"',$content);
            $content = str_replace('<IMG','<IMG class="gallery-thumb"',$content);    
        }


        $data['content'] = $content;
       
        $data['content'] = $content;

        if($data['KodeTopik']==5) {
              $data['jq1'] = true;  
        }

        return view('artikel',$data);

    }


    public function database($id = 0,$idsub = 0, $id_content = 0, $index = null) {

        $data = [];
        $data['topik'] = ApiServices::_reqTopikWithSub();
        $data['KodeTopik'] = $id;
        $data['KodeSubTopik'] = $idsub;
        $data['KodeArtikel'] = $id_content;
        $data['artikel'] = ApiServices::_reqArtikelWithKodeTopik($id);
        $data['subtopik'] = ApiServices::_getSubtopikByIdTopik($id);

        $data['artikelDetail'] = ApiServices::_getArtikelById($id,$idsub,$id_content);

        $artikelDetail = $data['artikelDetail'];



     $client = new Client();


     if($index == null) {
        $response = $client->request('GET', $artikelDetail->Result->Artikel_Database->Artikel_Database->DatabaseDataURL);
        $data['JudulReferensi'] = $artikelDetail->Result->Artikel_Database->Artikel_Database->JudulReferensi;
        $data['DatabaseOLAPURL'] = $artikelDetail->Result->Artikel_Database->Artikel_Database->DatabaseOLAPURL;
    }
     else {
         $dbArtikel = $artikelDetail->Result->Artikel_Database;
         $rowdb = $dbArtikel[$index];
         $response = $client->request('GET', $rowdb->DatabaseDataURL);

         $data['JudulReferensi'] = $rowdb->JudulReferensi;
         $data['DatabaseOLAPURL'] = $rowdb->DatabaseOLAPURL;
   
     }

     $ress = $response->getBody()->getContents();

     $ress = (string) $ress;    


        $ressArt = str_replace(': }',': 0 }',$ress);
        //$ressArt = str_replace('[','',$ressArt);
        //$ressArt = str_replace(']','',$ressArt);
        $ressArt = str_replace(': } ,',': 0 } ,',$ressArt);
        $ressArt = str_replace(':    } ,',': 0 } ,',$ressArt);
        $ressArt = str_replace('{  ','{',$ressArt);
        $ressArt = str_replace('   }','}',$ressArt);
        $ressArt = str_replace(' ,',',',$ressArt);
        $ressArt = str_replace('\r','',$ressArt);
        $ressArt = str_replace('\n','',$ressArt);
        $ressArt = str_replace('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">','',$ressArt);
       
        $data['data'] = $ressArt;


        if($index == null) {
            $response = $client->request('GET', $artikelDetail->Result->Artikel_Database->Artikel_Database->DatabaseSchemaURL);
        }
         else {
             $dbArtikel = $artikelDetail->Result->Artikel_Database;
             $rowdb = $dbArtikel[$index];
             $response = $client->request('GET', $rowdb->DatabaseSchemaURL);
       
         }

      
        $ress = $response->getBody()->getContents();
   
        $ress = (string) $ress;    
   
   
           $ressArt = str_replace(': }',': 0 }',$ress);
           $ressArt = str_replace(': } ,',': 0 } ,',$ressArt);
           $ressArt = str_replace(':    } ,',': 0 } ,',$ressArt);
           $ressArt = str_replace('{  ','{',$ressArt);
           $ressArt = str_replace('   }','}',$ressArt);
           $ressArt = str_replace(' ,',',',$ressArt);
           $ressArt = str_replace('\r','',$ressArt);
           $ressArt = str_replace('\n','',$ressArt);
           $ressArt = str_replace('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">','',$ressArt);
           
           $ressChart = (array) json_decode($ressArt);
           $ressArt = (array) json_decode($ressArt);
           
           $ressArt['dataSource'] = ['dataSourceType'=>'json','data'=>json_decode( $data['data'])];

           $data['report'] = json_encode($ressArt);
           $data['chart'] = json_encode($ressChart);

           

        return view('database',$data);

    }
}