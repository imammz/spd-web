<?php

namespace App\Helpers;
use DB;
use Log;
use Storage;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;



class ApiServices {

    public static function _url() {
        return 'http://202.78.202.37/XMLWebServiceSuprastructureSPD/SPDAPIService.aspx';
    }

    public static function _loadTopik() {
        $client = new Client();

        $req = '{
            "Request": { "ReqCode": "SPD_API_PortalTopik_Select" }
          }';

        $response = $client->request('POST', ApiServices::_url(), [
            'form_params' => [
                'request' => $req
                ]
            ]);

        return $response->getBody()->getContents();
    }

    public static function _loadSubTopik($id) {
        $client = new Client();

        $req = '{
            "Request": {
                            "ReqCode": "SPD_API_PortalSubTopik_Select",
                            "KodeTopik": '.$id.'
                        }
          }';

        $response = $client->request('POST', ApiServices::_url(), [
            'form_params' => [
                'request' => $req
                ]
            ]);

        return $response->getBody()->getContents();
    }




    public static function _reqTopikWithSub() {
        $topik = ApiServices::_loadTopik();
        $topik = str_replace('{Result :','{"Result" :',$topik);
        $topik = json_decode($topik);
        $topik = $topik->Result->Topik;

        $reqTopik = [];

        foreach($topik as $row) {
            $subTopik = ApiServices::_loadSubTopik($row->KodeTopik);

            $subTopik = str_replace('{Result :','{"Result" :',$subTopik);
            $subTopik = json_decode($subTopik);

            $subTopik = $subTopik->Result->SubTopik;

            $row->sub = $subTopik;

            $reqTopik[] = $row;
        }

        return $reqTopik;
    }


    public static function _loadArtikel($KodeTopik, $KodeSubTopik,$perPage = 20) {
        $client = new Client();

        $req = '
        {
            "Request": { "ReqCode": "SPD_API_PortalArtikel_Select",
          "KodeTopik":'.$KodeTopik.',
          "KodeSubTopik":'.$KodeSubTopik.',
          "PageIndex":1,
          "ArtikelPerPage":'.$perPage.'}
          }
        ';

        $response = $client->request('POST', ApiServices::_url(), [
            'form_params' => [
                'request' => $req
                ]
            ]);

        return $response->getBody()->getContents();
    }

    public static function _reqArtikelWithKodeTopik($KodeTopik) {
        $topik = ApiServices::_loadTopik();
        $topik = str_replace('{Result :','{"Result" :',$topik);
        $topik = json_decode($topik);
        $topik = $topik->Result->Topik;


            $subTopik = ApiServices::_loadSubTopik($KodeTopik);
            $subTopik = str_replace('{Result :','{"Result" :',$subTopik);
            $subTopik = json_decode($subTopik);

            $reqArtikel = [];
            foreach($subTopik->Result->SubTopik as $row) {

                $artikel = ApiServices::_loadArtikel($KodeTopik, $row->KodeSubTopik);
                $artikel = str_replace('{Result :','{"Result" :',$artikel);
                $artikel = json_decode($artikel);

                $rowArtikel = [];
                if(isset($artikel->Result->ARTIKEL)) {
                    $rowArtikel[] = $artikel->Result->ARTIKEL;
                }

/*
                foreach($artikel->Result->ARTIKEL as $rowArtikel) {
                    $rowArtikel[] = $rowArtikel;
                }
*/

                $row->artikel = $rowArtikel;
                $reqArtikel[] = $row;

            }


        return $reqArtikel;
    }



    public static function _getArtikelPage($KodeTopik, $KodeSubTopik,$ArtikelPerPage) {
        $client = new Client();

        $req = '
        {
            "Request": { "ReqCode": "SPD_API_PortalArtikel_FindPageCount",
          "KodeTopik":'.$KodeTopik.',
          "KodeSubTopik":'.$KodeSubTopik.'},
          "ArtikelPerPage":'.$ArtikelPerPage.'}
          }
        ';

        $response = $client->request('POST', ApiServices::_url(), [
            'form_params' => [
                'request' => $req
                ]
            ]);

        $ress = $response->getBody()->getContents();

           $ressArt = str_replace('{Result :','{"Result" :',$ress);
           $ressArt = json_decode($ressArt);

            return $ressArt;
    }


    public static function _getArtikelList($KodeTopik, $KodeSubTopik,$PageIndex,$ArtikelPerPage) {
        $client = new Client();

        $req = '
        {
            "Request": { "ReqCode": "SPD_API_PortalArtikel_Select",
            "KodeTopik":'.$KodeTopik.',          
            "KodeSubTopik":'.$KodeSubTopik.',          
            "PageIndex":'.$PageIndex.',
            "ArtikelPerPage":'.$ArtikelPerPage.'      
        }
          }
        ';

        $response = $client->request('POST', ApiServices::_url(), [
            'form_params' => [
                'request' => $req
                ]
            ]);

        $ress = $response->getBody()->getContents();

           $ressArt = str_replace('{Result :','{"Result" :',$ress);
           $ressArt = json_decode($ressArt);

            return $ressArt;
    }

    public static function _getArtikelById($KodeTopik, $KodeSubTopik,$KodeArtikel) {
        $client = new Client();

        /*
        $KodeTopik = 1;
        $KodeSubTopik = 1;
        $kodeArtikel = 1;
        */


        $req = '
        {
            "Request": { "ReqCode": "SPD_API_PortalArtikel_Detail",
          "KodeTopik":'.$KodeTopik.',
          "KodeSubTopik":'.$KodeSubTopik.',
          "KodeArtikel":'.$KodeArtikel.'}
          }
        ';

        $response = $client->request('POST', ApiServices::_url(), [
            'form_params' => [
                'request' => $req
                ]
            ]);

        $ress = $response->getBody()->getContents();

           $ressArt = str_replace('{Result :','{"Result" :',$ress);
           $ressArt = json_decode($ressArt);

            return $ressArt;
    }




    public static function _getSubtopikByIdTopik($KodeTopik) {
        $client = new Client();

        $req = '
        {
            "Request": { "ReqCode": "SPD_API_PortalSubTopik_Select",
          "KodeTopik":'.$KodeTopik.'}
          }
        ';

        $response = $client->request('POST', ApiServices::_url(), [
            'form_params' => [
                'request' => $req
                ]
            ]);

        $ress = $response->getBody()->getContents();

           $ressArt = str_replace('{Result :','{"Result" :',$ress);
           $ressArt = json_decode($ressArt);

            return $ressArt;
    }




    public static function _getLandingPage() {
        $client = new Client();

        $req = '
        {
            "Request": { "ReqCode": "SPD_API_PortalLandingPage_Select"}
          }
        ';

        $response = $client->request('POST', ApiServices::_url(), [
            'form_params' => [
                'request' => $req
                ]
            ]);

        $ress = $response->getBody()->getContents();

           $ressArt = str_replace('{Result :','{"Result" :',$ress);
           $ressArt = json_decode($ressArt);

            return $ressArt;
    }



    
}
