<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/    

Route::get('/','PublicController@index');

Route::get('/a',function(){
    echo 'Test';
});

Route::get('/topik/{id}/{KodeSubTopik?}','PublicController@topik');

Route::get('/topik/{id}/subtopik/{idsub}/artikel/{idkonten}','PublicController@artikel');

Route::get('/topik/{id}/subtopik/{idsub}/artikel/{idkonten}/database/{index?}','PublicController@database');


Route::get('/topik/{id}/artikel/{idkonten}','PublicController@artikel');

Route::get('/topik/{id}/berita/{idkonten}','PublicController@berita');
Route::get('/topik/{id}/makalah/{idkonten}','PublicController@makalah');
Route::get('/topik/{id}/database/{idkonten}','PublicController@database');
 
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    return "Cache is cleared";
});