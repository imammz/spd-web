@extends('layouts.app')

@section('header')

@php
   $img = '';
   $no = 1;
@endphp

@foreach($artikelDetail->Result->Artikel_Image as $row)
    @if($no==1)
  @php
    $img = $row->ArtikelImageURL;
@endphp  
    @endif

@php
   $no++;
@endphp
@endforeach

<meta name="description" content="{{ $artikelDetail->Result->ARTIKEL->JudulArtikel }}" />
<meta property="og:url"           content="{{ url()->full() }}" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="literasipolitik.org | {{ $artikelDetail->Result->ARTIKEL->JudulArtikel }}" />
<meta property="og:description"   content="{{ $artikelDetail->Result->ARTIKEL->JudulArtikel }}" />
<meta property="og:image"         content="{{$img}}" />

@endsection

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
   




    </div>

    <div class="main-content">


    <!--Department Details Page Start-->
    <div class="department-details">
       <div class="container">
          <div class="row">
             <div class="col-md-9">
                <!--Department Details Txt Start-->
                <div class="deprt-txt">


                     <h3> {{ $artikelDetail->Result->ARTIKEL->JudulArtikel }}  </h3>
                     

                  @if(!empty((array) $artikelDetail->Result->ARTIKEL->SubJudulArtikel))
                        <h4> {!! $artikelDetail->Result->ARTIKEL->SubJudulArtikel !!}  </h4>
                     @endif
                   
                           
                           <h6> 
                                 <br/>
                                 {{  date('d-F-Y', strtotime($artikelDetail->Result->ARTIKEL->TglPeristiwa)) }}
                              </h6>
                        <hr/>
                              
                        <div style="text-align: justify"> 
                             {!!  $content !!} 
                        </div>
                        

                        <hr/>

                        
                        <div class="other-department wf100">

                           @if($KodeTopik == 8)

                           @else
                              @if($KodeTopik == 5)
                                
                              @endif
                              <div class="row">
                                 <!--Department Box Start-->
                                 <div 
                                 @if($KodeTopik == 5)
                                    class="col-md-12"
                                 @else
                                    class="col-md-5"
                                 @endif
                                 >
                                  
                                    <div class="row">
                                       <div class="col-md-12">

                                    <div class="department-box mb30 c1">
                                       <h6>Basis Data</h6>
                                       <ul>
                                          @if(isset($artikelDetail->Result->Artikel_Database->Artikel_Database->DatabaseDataURL))

                                          <li><a href="#" onclick="openModal('{{ $artikelDetail->Result->Artikel_Database->Artikel_Database->DatabaseOLAPURL  }}','{{ $artikelDetail->Result->Artikel_Database->Artikel_Database->JudulReferensi  }}')"  ><i class="fas fa-circle"></i> {{ $artikelDetail->Result->Artikel_Database->Artikel_Database->JudulReferensi  }} </a></li>
                                       
                                          @else
                                                @foreach($artikelDetail->Result->Artikel_Database as $key=>$val)
                                                <li><a href="#"  onclick="openModal('{{ $val->DatabaseOLAPURL  }}','{{ $val->JudulReferensi  }}')" ><i class="fas fa-circle"></i> {{ $val->JudulReferensi  }} </a></li>
                                                @endforeach

                                        @endif
                                       </ul>
                                    </div>

                                       </div>

                                       <div class="col-md-12">

                                          <div class="department-box mb30 c1">
                                             <h6>Lampiran</h6>
                                             <ul>
                                                
                                                @if(isset($artikelDetail->Result->Artikel_PDF->Artikel_PDF->PDFRefURL))
                                                <li><a  href="{{ $artikelDetail->Result->Artikel_PDF->Artikel_PDF->PDFRefURL }}"><i class="fas fa-circle"></i> {{ $artikelDetail->Result->Artikel_PDF->Artikel_PDF->JudulMateri }}  </a></li>                                                  
                                                @else
                                                @foreach($artikelDetail->Result->Artikel_PDF as $val)
                                                    <li><a  href="{{ $val->PDFRefURL }}"><i class="fas fa-circle"></i> {{ $val->JudulMateri }}  </a></li>
                                                @endforeach

                                                @endif
                                             
                                              
                                             </ul>
                                          </div>
      
                                             </div>

                                    </div>


                                 </div>

                                 <!--Department Box End--> 
                                 <!--Department Box Start-->
                                 <div class="col-md-7">
                                      

 
                                          <div class="recent-event-block"> 
                                                <!--Slider Big Slider Start-->
                                                <div class="recent-event-slider">
                                    
                                                   @foreach($artikelDetail->Result->Artikel_Image as $row)
                                                 <div class="event-big">
                                                 
                                                    <img src="{{ $row->ArtikelImageURL }}" style="width: 458px; height: 400px;" alt=""> </div>
                                                    @endforeach
                                                 
                                                </div>
                                                <!--Slider Big Slider End--> 
                                                <!--Slider Big Slider Nav-->
                                                <div class="recent-event-slider-nav">
                                                      @foreach($artikelDetail->Result->Artikel_Image as $row)
                                                  <div><img src="{{ $row->ArtikelImageURL }}" style="width: 107px; height: 90px;" alt=""></div>
                                                    @endforeach
                                                </div>
                                                <!--Slider Big Slider Nav--> 
                                              </div>
                                       
                                   
                                 </div>
                                 <!--Department Box End--> 
                                 <!--Department Box Start-->
                                 
                                 <!--Department Box End--> 
                              </div>
                            
                           @endif   

                           </div>
                           <!--Related Departments End--> 



                </div>
                <!--Department Details Txt End-->
                <!--Department Details Share Start-->
                
                <div class="row">
                   <div class="col-lg-4">
                     <div class="share-post-single"> 
                        <div class="fb-share-button" 
                        data-href="{{ url()->full() }}" 
                        data-layout="button_count">
                      </div>
                     
                      &nbsp;
                      
                   </div>
                   <div class="col-lg-4">
                     <div class="fb-send" 
                     data-href="{{ url()->full() }}" 
                     data-layout="button_count">
                   </div>
                     &nbsp;

                   </div>
                   <div class="col-lg-4">

                     <a alt="Whatsapp" href="whatsapp://send" data-text="{{ $artikelDetail->Result->ARTIKEL->JudulArtikel }}" data-href="{{ url()->full() }}" class="whatsapp wa_btn"><span class="ion-social-whatsapp"></span> <img src="https://img.icons8.com/offices/30/000000/whatsapp.png"> </a>
             
                   </div>
                </div>   

             
                <br/>
               
                <br/>
              
              
               </div>
                <!--Department Details Share End-->
            
                
             </div>
             <!--Sidebar Start-->


             <div class="col-md-3">
                <div class="sidebar">
                     <!--Widget End--> 
                     <div class="widget">
                           <h4>Subtopik Lainnya</h4>
                             <div class="archives inner">
                               
                                 <ul>
                                       @foreach($subtopik->Result->SubTopik as $row)
                                       @if ($KodeSubTopik == $row->KodeSubTopik)
                                         @php
                                         $color = 'color: royalblue; background-color: #feeeee';
                                         @endphp  
                                       @else
                                       @php
                                          $color = '';
                                       @endphp  
                                       @endif
                                       <li><a href="{{ url('topik/'.$KodeTopik.'/'.$row->KodeSubTopik) }}" style="{{ $color }}">{{ $row->JudulSubTopik }}</a></li>
                                       @endforeach
         
         
                                      
                                   </ul>
                             </div>
                          </div>
                          <!--Widget End--> 
                     <!--Widget Start-->
                     <div class="widget">

                      @if($KodeTopik == 8)
                        <h4>Infografis Lainnya</h4>
                      @elseif($KodeTopik == 5)
                        <h4>Basis Data Lainnya</h4>
                      @else
                        <h4>Artikel Lainnya</h4>
                      @endif  
                       
                      
                      <div class="recent-posts inner">
                           <ul>
                             
                              @php
                              $no = 0;
                           @endphp

                                 @foreach($artikel as $rowArtikel)

                           

                                 @if(count($rowArtikel->artikel) == 0)
                     
                                 @else
                    
                                 @foreach ($rowArtikel->artikel as $rowItem2)
                             
                               @foreach($rowItem2 as $rowItem)
                             
                               @if(isset($rowItem->JudulArtikel) and $no <5)
                              <li>
                                 @if(isset($rowItem->ARTIKEL_THUMB))
                                 <img src="{{ $rowItem->ARTIKEL_THUMB }}" alt=""> 
                                 @else
                                 <img src="{{ asset('images/logo lp.png') }}" alt=""> 
                                 @endif

                                 @if(isset($rowItem->TglPeristiwa))
                                 <strong>{{ date('d-m-Y', strtotime($rowItem->TglPeristiwa))  }} </strong>

                                 @if(isset($rowItem->JudulArtikel))
                                 <h6> 
                                    <a href="{{ url('topik/'.$rowItem->KodeTopik.'/subtopik/'.$rowItem->KodeSubTopik.'/artikel/'.$rowItem->KodeArtikel.'') }}">{{ $rowItem->JudulArtikel }} </a> 
                                 </h6>
                                 @endif
                                 
                                 @endif
                              </li>
                              @endif

                              @php
                                 $no++;
                              @endphp

                              @endforeach
                              

                              
                              @endforeach
                              @endif

                             
                              @endforeach
                             

                              
                           </ul>
                        </div>
                     </div>
                     
                </div>
             </div>
             <!--Sidebar End-->
          </div>
       </div>
    </div>
    <!--Department Details Page End-->

    <div class="section-title-container">
                        
    
  
         


    <div class="row"> <br/> </div>


</div>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<script>
waShBtn = function() {
if( this.isIos === true ) {
var b = [].slice.call( document.querySelectorAll(".wa_btn") );
for (var i = 0; i < b.length; i++) {
  var t = b[i].getAttribute("data-text");
  var u = b[i].getAttribute("data-href");
  var o = b[i].getAttribute("href");
  var at = "?text=" + encodeURIComponent( t );
  if (t) {
      at += "%20%0A";
  }
  if (u) {
      at += encodeURIComponent( u );
  } else {
      at += encodeURIComponent( document.URL );
  }
  b[i].setAttribute("href", o + at);
  b[i].setAttribute("target", "_top");
  b[i].setAttribute("target", "_top");
  b[i].className += ' activeWhatsapp';
}
}
}

waShBtn.prototype.isIos = ((navigator.userAgent.match(/Android|iPhone/i) && !navigator.userAgent.match(/iPod|iPad/i)) ? true : false);

var theWaShBtn = new waShBtn();
</script>

<!-- Yose Modal CSS -->
<script src="{{ asset('yosemodal') }}/js/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('yosemodal') }}/css/yosemodal.css">
<!-- Gsap Animations -->
<script src="{{ asset('yosemodal') }}/js/gsap.js"></script>
<!-- Yose Modal JS -->
<script src="{{ asset('yosemodal') }}/js/yosemodal.js"></script>
<!-- Font Awesome  -->
<link rel="stylesheet" href="{{ asset('yosemodal') }}/font-awesome/css/font-awesome.min.css">


<script>

   function openModal(url,title) {

      $.yosemodal({
         title: title+" | BASIS DATA | literasipolitik.org", 
         iframe: url,
         loadingmessage: "sedang proses",
         loadingicon: "fa-gear",
         width: "100%",
         height: "100%",
         faicon: "fa-search",
         bgopacity: 0.7,
         });

   }

</script> 


@endsection
