@extends('layouts.app')

@section('content')

<div class="container-fluid">
   

    <div class="main-content">
    
    <div class="row justify-content-center">
      <!-- START REVOLUTION SLIDER -->
      <div class="rev_slider_wrapper fullwidthbanner-container collapse navbar-collapse">
        <div id="rev-slider2" class="rev_slider fullwidthabanner">
          <ul>
            @foreach($lp->Result->LandingPage as $rowItem)

              
            @if ($rowItem->ImageURL=='_')
            @php
                $rowItem->ImageURL = asset('images/bg.png');
            @endphp 
            @endif
            
            <li data-transition="fade"> <img src="{{ $rowItem->ImageURL }}"  alt="" width="1920" height="685" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
              <div class="tp-caption  tp-resizeme" style="vertical-align:bottom;"
              data-x="left" data-hoffset="400" 
              data-y="top" data-voffset="175" 
              data-transform_idle="o:1;"         
              data-transform_in="x:[-75%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
              data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
              data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
              data-splitin="none" 
              data-splitout="none"
              data-start="700">
                <div class="slide-content-box">
                  <div style=" font-size:28px; width:800px; min-height:40px;  background-color: rgb(10, 10, 10);
                  background-color: rgba(10, 10, 10, 0.5);
                  color: whitesmoke;
                  font-weight: bold;
                  border: 0px solid #fafafa;
                  z-index: 9;"> <br/>  &nbsp; {{ substr($rowItem->JudulBanner,0,90) }}... &nbsp; <br/> </div>
                </div>
              </div>

                                      
              <div class="tp-caption  tp-resizeme" 
              data-x="left" data-hoffset="400" 
              data-y="top" data-voffset="410" 
              data-transform_idle="o:1;"         
              data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
              data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
              data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
              data-splitin="none" 
              data-splitout="none"
              data-start="700">
                <div class="slide-content-box"> <a href="{{ url('topik/'.$rowItem->KodeTopikRef.'/subtopik/'.$rowItem->KodeSubTopikRef.'/artikel/'.$rowItem->KodeArtikelRef.'') }}" class="con">Selengkapnya</a> </div>
              </div>
            </li>


            @endforeach

          </ul>
        </div>
      </div>
      <!-- END REVOLUTION SLIDER --> 
    </div>

      <div class="row">



        <!--Local Boards & Services Start-->
        <section class="city-highlights">
          <div class="container">
          
            <div class="row m5">
              <div class="col-md-12 col-sm-8">
                  
                <div class="row">
               
                    <!--Cityscapes & Highlights Start-->
                    <section class="wf100" style="margin-top:15px; margin-bottom:15px;">
                      <div class="container-fluid">
                        <div id="highlight-slider" class="owl-carousel owl-theme"> 

                          @foreach($topik as $row)

                          @if($row->KodeTopik != 8)
                          <!--Item Start-->
                          <div class="item">
                            <div class="ch-box">
                              <div class="ch-thumb"> <a href="{{ url('/topik/'.$row->KodeTopik) }}"><i class="fas fa-link"></i></a> <img src="{{ $row->ImageTHUMB }}" style="width:380px; height:300px;" alt="{{ $row->Keterangan }}"> </div>
                              <div class="ch-txt">
                                <h6><a style="background-color: rgb(10, 10, 10);
                                  background-color: rgba(10, 10, 10, 0.5);
                                  color: whitesmoke;
                                  font-weight: bold;
                                  border: 2px solid #313131;
                                  z-index: 9;" class="text-white" href="{{ url('/topik/'.$row->KodeTopik) }}">  {{ $row->JudulTopik }} </a></h6>
                                <p>{{ $row->Keterangan }}</p>
                              </div>
                            </div>
                          </div>
                          <!--Item End--> 
                          @endif

                          @endforeach
                        
                        </div>
                      </div>
                    </section>
                    <!--Cityscapes & Highlights End--> 


                </div>
              </div>

             
            </div>
          </div>
        </section>
        <!--Local Boards & Services End-->



    </div>
  </div>
    <div class="row"> <br/> </div>


</div>

@endsection
