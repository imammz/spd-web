@extends('layouts.app')

@section('content')

<style>
      .chart-container {
         position: relative;
         height: 40vh;
         width: 40vw;
       }
       canvas {
         width: '400px';
         height: '400px';
       }
</style>

<div class="container-fluid">
    <div class="row justify-content-center">
   




    </div>

    <div class="main-content">


    <!--Department Details Page Start-->
    <div class="department-details">
       <div class="container">
          <div class="row">
             <div class="col-md-12">
                <!--Department Details Txt Start-->
                
                <div class="deprt-txt">
                
                              <h3>
                                    @if(isset($artikelDetail->Result->Artikel_Database->Artikel_Database->DatabaseDataURL))
                                    {{ $artikelDetail->Result->Artikel_Database->Artikel_Database->JudulReferensi  }}
                                    @endif
                                    
                                    {{ $JudulReferensi }}
                              </h3>

                               <h6> 
                                    {{ $artikelDetail->Result->ARTIKEL->JudulArtikel }}
                                  &nbsp;  |  &nbsp;
                                   {{  date('d-F-Y', strtotime($artikelDetail->Result->ARTIKEL->TglPeristiwa)) }}
                                </h6>

                                <hr/>
                                <div style="text-align: center"> 
                                        <a target="_blank" href="{{ $DatabaseOLAPURL }}">
                                          Click For Full Screen
                                        </a>       
                                </div>

                                <iframe src="{{ $DatabaseOLAPURL }}" width="100%" height="500" frameBorder="0" scrolling="yes">
                                </iframe > 

                                <hr/>


                </div>
                <!--Department Details Txt End-->
                <!--Department Details Share Start-->
                
                <div class="row">
                    <div class="col-lg-4">
                      <div class="share-post-single"> 
                         <div class="fb-share-button" 
                         data-href="{{ url()->full() }}" 
                         data-layout="button_count">
                       </div>
                      
                       &nbsp;
                       
                    </div>
                    <div class="col-lg-4">
                      <div class="fb-send" 
                      data-href="{{ url()->full() }}" 
                      data-layout="button_count">
                    </div>
                      &nbsp;
 
                    </div>
                    <div class="col-lg-4">
 
                      <a alt="Whatsapp" href="whatsapp://send" data-text="{{ $artikelDetail->Result->ARTIKEL->JudulArtikel }}" data-href="{{ url()->full() }}" class="whatsapp wa_btn"><span class="ion-social-whatsapp"></span> <img src="https://img.icons8.com/offices/30/000000/whatsapp.png"> </a>
              
                    </div>
                 </div>  
             
                <!--Department Details Share End-->
            
                
             </div>

             <div class="col-md-12">
                  


             </div>
        
          </div>
       </div>
    </div>
    <!--Department Details Page End-->





</div>


<link href="https://cdn.webdatarocks.com/latest/webdatarocks.min.css" rel="stylesheet"/>
<script src="https://cdn.webdatarocks.com/latest/webdatarocks.toolbar.min.js"></script>
<script src="https://cdn.webdatarocks.com/latest/webdatarocks.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<script>
waShBtn = function() {
if( this.isIos === true ) {
var b = [].slice.call( document.querySelectorAll(".wa_btn") );
for (var i = 0; i < b.length; i++) {
  var t = b[i].getAttribute("data-text");
  var u = b[i].getAttribute("data-href");
  var o = b[i].getAttribute("href");
  var at = "?text=" + encodeURIComponent( t );
  if (t) {
      at += "%20%0A";
  }
  if (u) {
      at += encodeURIComponent( u );
  } else {
      at += encodeURIComponent( document.URL );
  }
  b[i].setAttribute("href", o + at);
  b[i].setAttribute("target", "_top");
  b[i].setAttribute("target", "_top");
  b[i].className += ' activeWhatsapp';
}
}
}

waShBtn.prototype.isIos = ((navigator.userAgent.match(/Android|iPhone/i) && !navigator.userAgent.match(/iPod|iPad/i)) ? true : false);

var theWaShBtn = new waShBtn();
</script>

@if(false)

<script>

 
    
    var pivot = new WebDataRocks({
            container: "#wdr-component",
            beforetoolbarcreated: customizeToolbar,
            toolbar: true,
            report: {!! $report !!},
            reportcomplete: function() {
               pivot.off("reportcomplete");
               createPolarChart();
            }
        });

        var chart;

        function createPolarChart() {
            webdatarocks.getData({
                "slice": {!! $chart !!},
            }, drawChart, updateChart);
        }
        
        function prepareDataFunction(rawData) {
            var result = {};
            var labels = [];
            var data = [];
            for (var i = 0; i < rawData.data.length; i++) {
                var record = rawData.data[i];
                if (record.c0 == undefined && record.r0 !== undefined) {
                    var _record = record.r0;
                    labels.push(_record);
                }
                if (record.c0 == undefined & record.r0 == undefined) continue;
                if (record.v0 != undefined) {
                    data.push(!isNaN(record.v0) ? record.v0 : null);
                }
            }
            result.labels = labels;
            result.data = data;
            return result;
        }
        
        function drawChart(rawData) {
            var data = prepareDataFunction(rawData);
            var data_for_charts = {
                datasets: [{
                    data: data.data,
                    backgroundColor: [
                        "#6384FF",
                        "#4BC0C0",
                        "#FFCE56",
                        "#E7E9ED",
                        "#36A2EB",
                        "#9ccc65",
                        "#b3e5fc"
        
                    ]
                }],
                labels: data.labels
            };
            options = {
                responsive: true,
                legend: {
                    position: 'right',
                },
                title: {
                    display: true,
                    fontSize: 18,
                    text: '{{ $artikelDetail->Result->Artikel_Database->Artikel_Database->JudulReferensi  }}'
                },
                scale: {
                    ticks: {
                        //beginAtZero: true
                    },
                    reverse: false
                },
                animation: {
                    animateRotate: false,
                    animateScale: false
                }
            };
            var ctx = document.getElementById("polar-chart").getContext("2d");
            chart = new Chart(ctx, {
                data: data_for_charts,
                type: 'bar',
                options: {
                    scales: {
                        xAxes: [{
                            stacked: false
                        }],
                        yAxes: [{
                            stacked: false
                        }]
                    },
                    responsive: true,
                legend: {
                    position: 'right',
                },
                title: {
                    display: true,
                    fontSize: 18,
                    text: '{{ $artikelDetail->Result->Artikel_Database->Artikel_Database->JudulReferensi  }}'
                }



                },
                title: {
                    display: true,
                    fontSize: 18,
                    text: '{{ $artikelDetail->Result->Artikel_Database->Artikel_Database->JudulReferensi  }}'
                }
            });
        }
        
        function updateChart(rawData) {
            chart.destroy();
            drawChart(rawData);
        }
        
        

        function customizeToolbar(toolbar) {
         var tabs = toolbar.getTabs(); // get all tabs from the toolbar
        
         tabs3 = tabs[3];

         toolbar.getTabs = function() {
             delete tabs[0]; 
             delete tabs[1]; 
             delete tabs[2]; 
             delete tabs[3].menu[1];
             delete tabs[3].menu[2];

             return tabs;
         }
     }
        </script>



     
        
 @endif


@endsection
