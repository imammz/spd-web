@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                    <!--Slider Start-->
                    <div class="main-slider wf100">
                      <div id="home-slider" class="owl-carousel owl-theme">
                        @php
                            $no = 0;
                        @endphp
                     

                      </div>
                    </div>
                    <!--Slider End-->


            </div>
        </div>





    </div>

    <div class="main-content">
       
         @foreach($topik as $row)
         @if ($row->KodeTopik == $KodeTopik)
    <div class="text-center"> 
         <strong> 
            <br/>
            <br/>
            <h3>  {{ $row->JudulTopik  }} </h2>
               <br/>
            </strong>    
   </div>
    @endif
    @endforeach
    <div class="row">
         <div class="col-lg-9">

          
            @foreach($artikel as $rowItem)
        
            @if ($rowItem->ARTIKEL_THUMB=='_')
            @php
                $rowItem->ARTIKEL_THUMB = asset('images/bg.png');
            @endphp 
            @endif

            <!--News Box Start-->
            <div class="col-md-4 col-sm-6">
               <div class="news-post image-post">
                  <div class="thumb">
                     <img style="height: 250px;" src="{{ $rowItem->ARTIKEL_THUMB }}" alt="{{ $rowItem->Keterangan }}">
                  </div>
                  <div class="news-post-txt">
                     <span class="ecat c1">{{ $rowItem->SubJudulArtikel }}</span> 
                     <!--Share Start-->
                     <div class="btn-group share-post">
                        <button type="button" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-share-alt"></i> Share </button>
                        <ul class="dropdown-menu">
                           <li><a href="#" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                           <li><a href="#" class="tw"><i class="fab fa-twitter"></i></a></li>
                           <li><a href="#" class="insta"><i class="fab fa-instagram"></i></a></li>
                           <li><a href="#" class="linken"><i class="fab fa-linkedin-in"></i></a></li>
                           <li><a href="#" class="yt"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                     </div>
                     <!--Share End-->
                     <h6><a style="color: whitesmoke; text-align: justify; text-shadow: 2px 2px #222222;" href="{{ url('topik/'.$rowItem->KodeTopik.'/subtopik/'.$rowItem->KodeSubTopik.'/artikel/'.$rowItem->KodeArtikel.'') }}">{{ $rowItem->JudulArtikel }}</a>
                     </h6>
                     <ul class="news-meta">

                        <li><i class="far fa-calendar-alt"></i> {{ date('d-m-Y', strtotime($rowItem->TglPeristiwa))  }}</li>
                     </ul>
                  </div>
               </div>
            </div>
            <!--News Box End-->
            @endforeach

         
         </div>
        
         <div class="col-md-3">
            <div class="sidebar">
                 <!--Widget End--> 
                 <div class="widget">
                       <h4>Subtopik Lainnya</h4>
                         <div class="archives inner">
                           
                             <ul>
                                   @foreach($subtopik->Result->SubTopik as $row)
                                   @if ($KodeSubTopik == $row->KodeSubTopik)
                                     @php
                                     $color = 'color: royalblue; background-color: #feeeee';
                                     @endphp  
                                   @else
                                   @php
                                      $color = '';
                                   @endphp  
                                   @endif
                                   <li><a href="{{ url('topik/'.$KodeTopik.'/'.$row->KodeSubTopik) }}" style="{{ $color }}">{{ $row->JudulSubTopik }}</a></li>
                                   @endforeach
     
     
                                  
                               </ul>
                         </div>
                      </div>
                      <!--Widget End--> 
               
                 
            </div>
         </div>

       </div>

       @if(false)
       <div class="row">
          <div class="site-pagination">
             <nav aria-label="Page navigation">
                <ul class="pagination">
                   <li> <a href="#" aria-label="Previous"> <span aria-hidden="true">«</span> </a> </li>
                   <li><a href="#">1</a></li>
                   <li><a href="#">2</a></li>
                   <li class="active"><a href="#">3</a></li>
                   <li><a href="#">4</a></li>
                   <li><a href="#">5</a></li>
                   <li> <a href="#" aria-label="Next"> <span aria-hidden="true">»</span> </a> </li>
                </ul>
             </nav>
          </div>
       </div>
       @endif

  </div>
    <div class="row"> <br/> </div>


</div>

@endsection
