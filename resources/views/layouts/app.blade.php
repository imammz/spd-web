<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="{{ url('images/logo.png') }}"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    @yield('header')

    <!-- Scripts -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <style>
      .whatsapp{
      display: none;
      color: #1DC143;
    }
    
    .whatsapp:hover, .whatsapp:focus{
      background: #1DC143;
      color: #fff;
      text-shadow: 0 0 1px rgba(0,0,0,0.2);
    }
    
    .whatsapp.activeWhatsapp{
      display: inline-block;
    }
      </style>

<!-- CSS Files -->
<link href="{{ asset('themes/css/custom.css')}}" rel="stylesheet">
<link href="{{ asset('themes/css/responsive.css')}}" rel="stylesheet">
<link href="{{ asset('themes/css/color.css')}}" rel="stylesheet">
<link href="{{ asset('themes/css/all.css')}}" rel="stylesheet">
<link href="{{ asset('themes/css/owl.carousel.min.css')}}" rel="stylesheet">
<link href="{{ asset('themes/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{ asset('themes/css/prettyPhoto.css')}}" rel="stylesheet">
<link href="{{ asset('themes/css/slick.css')}}" rel="stylesheet">

<link rel="stylesheet" href="{{ asset('themes') }}/js/rev-slider/css/settings.css"  type='text/css' media='all' />
<link rel="stylesheet" href="{{ asset('themes') }}/js/rev-slider/css/layers.css"  type='text/css' media='all' />
<link rel="stylesheet" href="{{ asset('themes') }}/js/rev-slider/css/navigation.css"  type='text/css' media='all' />


 

<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->


      <link href="{{ asset('webdata') }}/webdatarocks.min.css" rel="stylesheet"/>


<style>
        footer.nb-footer {
            background: #222;
            border-top: 4px solid #b78c33; }
            footer.nb-footer .about {
            margin: 0 auto;
            margin-top: 40px;
            max-width: 1170px;
            text-align: center; }
            footer.nb-footer .about p {
            font-size: 13px;
            color: #999;
            margin-top: 30px; }
            footer.nb-footer .about .social-media {
            margin-top: 15px; }
            footer.nb-footer .about .social-media ul li a {
            display: inline-block;
            width: 45px;
            height: 45px;
            line-height: 45px;
            border-radius: 50%;
            font-size: 16px;
            color: #b78c33;
            border: 1px solid rgba(255, 255, 255, 0.3); }
            footer.nb-footer .about .social-media ul li a:hover {
            background: #b78c33;
            color: #fff;
            border-color: #b78c33; }
            footer.nb-footer .footer-info-single {
            margin-top: 30px; }
            footer.nb-footer .footer-info-single .title {
            color: #aaa;
            text-transform: uppercase;
            font-size: 16px;
            border-left: 4px solid #b78c33;
            padding-left: 5px; }
            footer.nb-footer .footer-info-single ul li a {
            display: block;
            color: #aaa;
            padding: 2px 0; }
            footer.nb-footer .footer-info-single ul li a:hover {
            color: #b78c33; }
            footer.nb-footer .footer-info-single p {
            font-size: 13px;
            line-height: 20px;
            color: #aaa; }
            footer.nb-footer .copyright {
            margin-top: 15px;
            background: #111;
            padding: 7px 0;
            color: #999; }
            footer.nb-footer .copyright p {
            margin: 0;
            padding: 0; }
</style>

</head>
<body>
    <div id="app">


        <header class="wf100 header">
            <div class="topbar">
              <div class="container">
                <div class="row">
                  
                  <div class="col-md-8 col-sm-8">

                    <ul class="left-links">
                      <!--
                            <li> <a href="#"> <i class="fa fa-calendar"></i> Kalender Even</a> </li>
                      -->
                      <li> <a href="{{ url('/') }}"> <span>  <i class="fa fa-home"></i> </span>   </a> </li>

                </ul>
                  </div>
                  <div class="col-md-4 col-sm-4">
                    <ul class="right-links">
                            <li> <a href="#"><strong> {{ date('d-F-Y') }}</strong></a> </li>
                            <li> <a href="#"><i class="fas fa-street-view"></i> Jelajah Informasi</a> </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="logo-nav-row">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-12">
                    <nav class="navbar">
                      
                      <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                        <a class="navbar-brand" href="{{ url('') }}"><img src="{{ asset('images/logo lp.png') }}" width="220px" alt=""></a>
                      
                    </div>
                      <!-- Collect the nav links, forms, and other content for toggling -->
                      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                          @php
                            $no=0;
                          @endphp

                          @foreach ($topik as $row)
                          @if($no<6 and $row->KodeTopik != 6 and $row->KodeTopik !=8)
                          <li class="dropdown"> <a href="{{ url('/topik/'.$row->KodeTopik) }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
                            {{ $row->JudulTopik }}
                            <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                                
                            @foreach($row->sub as $rowSub)
                            @if(isset($rowSub->KodeTopik) and isset($row->KodeTopik)) 
                              @if ($rowSub->KodeTopik == $row->KodeTopik)
                              <li>
                                      <a  href="{{ url('topik/'.$row->KodeTopik.'/'.$rowSub->KodeSubTopik) }}">{{  $rowSub->JudulSubTopik }}</a>
                                   </li>

                              @endif
                             @endif      
                               
                             @endforeach
                      
                            </ul>
                          </li>
                          @endif
                          @php
                          $no++;
                        @endphp
                          @endforeach

                          @foreach ($topik as $row)
                          @if($row->KodeTopik == 8)
                          <li> <a href="{{ url('/topik/'.$row->KodeTopik) }}"   role="button" aria-haspopup="true" aria-expanded="false"> 
                           <i class="fas fa-image"></i> {{ $row->JudulTopik }} </a>
                          </li>
                          @endif

                          @endforeach

                        </ul>

                        <ul class="nav navbar-nav navbar-right">
                          <li class="search-btn"><a class="search-icon" href="#search"> <i class="fas fa-search"></i> </a></li>
                          

                       
                          


                        </ul>
                        <div id="search">
                          <button type="button" class="close">×</button>
                          <form class="search-overlay-form">
                            <input type="search" value="" placeholder="Informasi yang anda cari ?" />
                            <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                          </form>
                        </div>
                      </div>
                    </nav>
                  </div>
                </div>
              </div>
            </div>
          </header>

        <main class="py-4">
            @yield('content')
        </main>
    </div>


    <footer class="nb-footer">
            <div class="container">
            <div class="row">
            <div class="col-sm-12">
            <div class="about">
            

            </div>
            </div>

            <div class="col-md-3 col-sm-6">
            <div class="footer-info-single">
                <ul class="list-unstyled">
                    <li><a href="#" title=""><i class="fa fa-angle-double-right"></i> Daftar Istilah</a></li>
                    <li><a href="#" title=""><i class="fa fa-angle-double-right"></i> Sitemap</a></li>
                  <li> <br/> </li>
                    <li><a href="#" title=""><i class="fa fa-address-card"></i> Jl. Proklamasi No, 65 A Menteng,<br/>Jakarta Pusat 10320 </a></li>
                    <li><a href="#" title=""><i class="fa fa-phone"></i> (021) - 390 6072 </a></li>
                    <li>
                      <div>
                    <span href="#" title=""><i class="fab fa-facebook-f"></i> </span> &nbsp; &nbsp; &nbsp; <span href="#" title=""><i class="fab fa-twitter"></i>  </span> &nbsp; &nbsp; &nbsp; <span href="#" title=""><i class="fa fa-envelope-open-text"></i>  </span>
                  </div>
                  </li>
                </ul>
            </div>
            </div>

            <div class="col-md-5 col-sm-6">
            <div class="footer-info-single">
                <h2 class="title">Informasi</h2>
                <ul class="list-unstyled">
                    <li><a href="#" title=""><i class="fa fa-angle-double-right"></i> Tentang Kami</a></li>
                    <li><a href="#" title=""><i class="fa fa-angle-double-right"></i> Hubungi Kami</a></li>
                  

                          
                </ul>
            </div>
            </div>



            <div class="col-md-2 col-sm-6">
                    <div class="footer-info-single">

                      <h2 class="title"> Supported By </h2> <br/>
                      <a href="https://www.tifafoundation.org/" target="_blank">
                        <img src="{{ asset('images/LOGO TIFA WHITE HR.png') }}" width="130px" class="img-responsive center-block" alt="">
                      </a>
                    </div>
                    </div>

                    <div class="col-md-2 col-sm-6">
                        <div class="footer-info-single">

                            <h2 class="title"> Organized By </h2> <br/>
                            <a href="https://www.spd-indonesia.com/" target="_blank">
                            <img src="{{ asset('images/logo.png') }}" width="130px" class="img-responsive center-block" alt="">
                           </a>
                        </div>
                        </div>



            </div>
            </div>

            <section class="copyright">
            <div class="container">
            <div class="row">
            <div class="col-sm-6">
            <p>Copyright © 2019. Sindikasi Demokrasi Dan Pemilu.</p>
            </div>
            <div class="col-sm-6"></div>
            </div>
            </div>
            </section>
            </footer>


            <!-- JS -->
            @if (isset($jq1))
            &nbsp;                
            @else
            <script src="{{ asset('themes/js/jquery.min.js')}}"></script>
            @endif
            <script src="{{ asset('themes/js/bootstrap.min.js')}}"></script>
            <script src="{{ asset('themes/js/owl.carousel.min.js')}}"></script>
            <script src="{{ asset('themes/js/jquery.prettyPhoto.js')}}"></script>
            <script src="{{ asset('themes/js/slick.min.js')}}"></script>
            <script src="{{ asset('themes/js/custom.js')}}"></script>
            <!--Rev Slider Start-->
            <script type="text/javascript" src="{{ asset('themes') }}/js/rev-slider/js/jquery.themepunch.tools.min.js"></script> 
            <script type="text/javascript" src="{{ asset('themes') }}/js/rev-slider/js/jquery.themepunch.revolution.min.js"></script> 
            <script type="text/javascript" src="{{ asset('themes') }}/js/rev-slider.js"></script> 
            <script type="text/javascript" src="{{ asset('themes') }}/js/rev-slider/js/extensions/revolution.extension.actions.min.js"></script> 
            <script type="text/javascript" src="{{ asset('themes') }}/js/rev-slider/js/extensions/revolution.extension.carousel.min.js"></script> 
            <script type="text/javascript" src="{{ asset('themes') }}/js/rev-slider/js/extensions/revolution.extension.kenburn.min.js"></script> 
            <script type="text/javascript" src="{{ asset('themes') }}/js/rev-slider/js/extensions/revolution.extension.layeranimation.min.js"></script> 
            <script type="text/javascript" src="{{ asset('themes') }}/js/rev-slider/js/extensions/revolution.extension.migration.min.js"></script> 
            <script type="text/javascript" src="{{ asset('themes') }}/js/rev-slider/js/extensions/revolution.extension.navigation.min.js"></script> 
            <script type="text/javascript" src="{{ asset('themes') }}/js/rev-slider/js/extensions/revolution.extension.parallax.min.js"></script> 
            <script type="text/javascript" src="{{ asset('themes') }}/js/rev-slider/js/extensions/revolution.extension.slideanims.min.js"></script> 
            <script type="text/javascript" src="{{ asset('themes') }}/js/rev-slider/js/extensions/revolution.extension.video.min.js"></script>


    <script src="{{ asset('webdata') }}/webdatarocks.toolbar.min.js"></script>
    <script src="{{ asset('webdata') }}/webdatarocks.js"></script>
    <script src="https://cdn.webdatarocks.com/latest/webdatarocks.toolbar.min.js"></script>


</body>
</html>
